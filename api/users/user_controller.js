const { create, getByEmail, get } = require('./user_model');
const { genSaltSync, hashSync } = require('bcrypt');

module.exports = {
    createUser: (req, res) => {
        const body = req.body;
        if (Object.keys(body).length === 0) {
            return res.status(400).json({
                code: 1,
                message: "You're not send with apropriate format!"
            });
        }
        const salt = genSaltSync(10);
        body.password = hashSync(body.password, salt);
        getByEmail(body, (err, results) => {
            if (err) {
                console.log(err);
                return res.status(500).json({
                    code: 1,
                    message: process.env.MSG_DB_CON_ERROR
                });
            } else {
                if (results.length !== 0) {
                    return res.status(400).json({
                        code: 1,
                        message: "Email already registered"
                    });
                } else {
                    create(body, (err, results) => {
                        if (err) {
                            return res.status(500).json({
                                code: 1,
                                message: process.env.MSG_DB_CON_ERROR
                            });
                        }
                        return res.status(200).json({
                            code: 0,
                            message: "Success add new register"
                        });
                    });
                }
            }
        });
    },
    getUser: (req, res) => {
        get((err, results) => {
            if (err) {
                console.log(err);
                return res.status(500).json({
                    code: 1,
                    message: process.env.MSG_DB_CON_ERROR
                });
            } else {
                return res.status(200).json({
                    code: 0,
                    message: "Success get user data",
                    data: results
                });
            }
        });
    }
}