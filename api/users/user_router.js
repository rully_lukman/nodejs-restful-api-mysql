const { createUser, getUser } = require('./user_controller');
const router = require('express').Router();

router.post("/", createUser);
router.get("/", getUser);

module.exports = router;