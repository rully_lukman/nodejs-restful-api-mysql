const pool = require('../../config/database');


module.exports = {
    getByEmail: (data, callBack) => {
        pool.query(
            `SELECT id FROM registration WHERE email = ?`,
            [
                data.email
            ],
            (error, result, fields) => {
                if (error) {
                    return callBack(error)
                }
                return callBack(null, result);
            }
        );
    },
    create: (data, callBack) => {
        pool.query(
            `INSERT INTO registration (firstName, lastName, gender, email, password, number) VALUES (?,?,?,?,?,?)`,
            [
                data.firstName,
                data.lastName,
                data.gender,
                data.email,
                data.password,
                data.number,
            ],
            (error, result, fields) => {
                if (error) {
                    return callBack(error)
                }
                return callBack(null)
            }
        );
    },
    get: (callBack) => {
        pool.query(
            `SELECT id, firstName, lastName, gender, email, number FROM registration`,
            (error, result, fields) => {
                if (error) {
                    return callBack(error)
                }
                return callBack(null, result)
            }
        );
    }
};