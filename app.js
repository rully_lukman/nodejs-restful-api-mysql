const express = require('express');
const app = express();
const morgan = require('morgan'); // logger middleware
const bodyParser = require('body-parser');
const multer = require('multer');
const upload = multer();
const userRouter = require("./api/users/user_router");
const jwt = require('jsonwebtoken')

app.use(morgan('dev'));

// for parsing application/xwww-
app.use(bodyParser.urlencoded({
    extended: true
}));

// for parsing application/json
app.use(bodyParser.json());

// for parsing multipart/form-data
app.use(upload.array());
app.use(express.static('public'));

// Handling CORS
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');

    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
        return res.status(200).json({});
    }

    // testing jwt token
    const token = jwt.sign({ foo: 'bar', iat: Math.floor(Date.now() / 1000) + (60 * 60), }, 'shhhhh');
    console.log(`token`, token)
    jwt.verify(token, 'shhhhh', (err, decoded) => {
        err ? console.log(err) : console.log(decoded)
    })


    next();
});

// Calling All Router
app.use("/api/users", userRouter);


// Handling Error/Empty Endpoint
app.use((req, res, next) => {
    const error = new Error("You're going nowhere");
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        code: 1,
        message: error.message
    });
});


module.exports = app;